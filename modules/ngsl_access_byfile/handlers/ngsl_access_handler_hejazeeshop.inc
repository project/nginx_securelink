<?php

/**
 * @author "Ahmad Hesamzadeh" <mngafa@gmail.com>
 * @file Access handler which is based on HejazeeShop orders
 *
 * Provides ability to set access control according to HejazeeShop orders.
 * So you can sell files. Only users that have ordered the product, can request download link
 */

/**
 * Access handler callback for handler "hejazeeshop"
 *
 * If the current user has purchased the given HejazeeShop product, return TRUE. Otherwise return FALSE.
 * In case of errors, it will also return FALSE.
 *
 * NOTE: Order's SerialKey should be passed as $_GET['serialkey'] (First option)
 *  or be available in $_SESSION['hejazeeshop_serialkey'] (Fallback)
 *
 * @param $product_sku The SKU of the product
 *
 * @return bool
 */
function ngsl_access_byfile_handler_hejazeeshop($product_sku) {
  // This handler requires the hejazeeshop.module
  // So if it's not present, we don't have anything to do.
  if (!module_exists('hejazeeshop')) {
    watchdog('nginx_securelink', 'ngsl_access_byfile_handler_hejazeeshop: Module "hejazeeshop" is not installed. You need this module for granting access to files based on hejazeeshop orders. If yu don\'t install hejazeeshop, all access request that are passed to this handler, will be rejected.', array(), WATCHDOG_WARNING);

    return FALSE;
  }

  // Fetch serialkey
  if (isset($_GET['serialkey'])) {
    $serialkey = $_GET['serialkey'];
  }
  elseif (isset($_SESSION['hejazeeshop_serialkey'])) {
    $serialkey = $_SESSION['hejazeeshop_serialkey'];
  }
  else {
    // SerialKey is not available
    drupal_set_message(t('SerialKey is not available.'), 'error');

    return FALSE;
  }

  // Validate serialkey format
  $serial_pattern = '/^[0-9a-z]{4}\-[0-9a-z]{4}\-[0-9a-z]{4}\-[0-9a-z]{4}$/i';
  if (!preg_match($serial_pattern, $serialkey)) {
    // Does not match
    drupal_set_message(t('Your serial key is incorrectly formatted. Example of a valid serial is: ABCD-EFGH-1234-5678'), 'error');

    return FALSE;
  }

  // Order should be placed not before this time
  // Set it to zero: All order will be valid.
  $oldest_valid_time = 0;

  // Look for paid orders with the given serial key and product SKU and in a valid time.
  $query = db_select('hejazeeshop_orders', 'hso');
  $query->fields('hso', array('oid'));

  $query->condition("hso.product", $product_sku);
  $query->condition('hso.serialkey', $serialkey);
  $query->condition('hso.created', $oldest_valid_time, '>=');

  // Only completed (paid) orders
  $query->condition('hso.status', 1);

  $result = $query->execute()->fetchAll();

  return !empty($result);
}
