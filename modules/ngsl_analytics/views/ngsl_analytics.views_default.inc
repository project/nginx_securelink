<?php

/**
 * @file
 * Define default views for ngsl_analytics.module
 *
 * @author Ahmad Hejazee <mngafa@gmail.com>
 */

/**
 * Implements hook_views_default_views().
 */
function ngsl_analytics_views_default_views() {
  $path = dirname(__FILE__) . '/default_views/';
  $views = array();

  // Add View
  require_once $path . 'nginx_securelink_analytics.views_export.inc';
  $views[$view->name] = $view;

  // Add View
  require_once $path . 'nginx_securelink_top_downloads.views_export.inc';
  $views[$view->name] = $view;

  return $views;
}
