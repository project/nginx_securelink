<?php
$view = new view();
$view->name = 'nginx_securelink_analytics';
$view->description = 'Analytics for nginx securelink';
$view->tag = 'default';
$view->base_table = 'ngsl_analytics';
$view->human_name = 'Nginx securelink analytics';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Nginx securelink analytics';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access ngsl analytics';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'created' => 'created',
  'name' => 'name',
  'requested_path' => 'requested_path',
  'referrer' => 'referrer',
);
$handler->display->display_options['style_options']['default'] = 'created';
$handler->display->display_options['style_options']['info'] = array(
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'requested_path' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'referrer' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 1,
  ),
);
/* Relationship: Nginx securelink: NGSL user */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'ngsl_analytics';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
/* Field: Nginx securelink: Request date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Date';
$handler->display->display_options['fields']['created']['date_format'] = 'medium';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'User';
/* Field: Nginx securelink: Requested path */
$handler->display->display_options['fields']['requested_path']['id'] = 'requested_path';
$handler->display->display_options['fields']['requested_path']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['requested_path']['field'] = 'requested_path';
$handler->display->display_options['fields']['requested_path']['label'] = 'Path';
/* Field: Nginx securelink: Request referrer */
$handler->display->display_options['fields']['referrer']['id'] = 'referrer';
$handler->display->display_options['fields']['referrer']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['referrer']['field'] = 'referrer';
$handler->display->display_options['fields']['referrer']['label'] = 'Referrer';
/* Filter criterion: Nginx securelink: Requested path */
$handler->display->display_options['filters']['requested_path']['id'] = 'requested_path';
$handler->display->display_options['filters']['requested_path']['table'] = 'ngsl_analytics';
$handler->display->display_options['filters']['requested_path']['field'] = 'requested_path';
$handler->display->display_options['filters']['requested_path']['operator'] = 'contains';
$handler->display->display_options['filters']['requested_path']['exposed'] = TRUE;
$handler->display->display_options['filters']['requested_path']['expose']['operator_id'] = 'requested_path_op';
$handler->display->display_options['filters']['requested_path']['expose']['label'] = 'Requested path contains';
$handler->display->display_options['filters']['requested_path']['expose']['description'] = 'Only show analytics for certain files.';
$handler->display->display_options['filters']['requested_path']['expose']['operator'] = 'requested_path_op';
$handler->display->display_options['filters']['requested_path']['expose']['identifier'] = 'requested_path';
$handler->display->display_options['filters']['requested_path']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
/* Filter criterion: User: Name */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['relationship'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['label'] = 'Show downloads for certain users';
$handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
$handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['defaults']['exposed_form'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
$handler->display->display_options['path'] = 'admin/reports/ngsl/analytics';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'NGSL Analytics';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
