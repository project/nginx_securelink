<?php
$view = new view();
$view->name = 'nginx_securelink_top_downloads';
$view->description = 'Show top downloaded files.';
$view->tag = 'default';
$view->base_table = 'ngsl_analytics';
$view->human_name = 'Nginx securelink top downloads';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Top downloads by Nginx securelink';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['group_by'] = TRUE;
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'access ngsl analytics';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '30';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'requested_path' => 'requested_path',
  'created' => 'created',
  'created_1' => 'created_1',
  'created_2' => 'created_2',
);
$handler->display->display_options['style_options']['default'] = 'created';
$handler->display->display_options['style_options']['info'] = array(
  'requested_path' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created_2' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Field: Nginx securelink: Requested path */
$handler->display->display_options['fields']['requested_path']['id'] = 'requested_path';
$handler->display->display_options['fields']['requested_path']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['requested_path']['field'] = 'requested_path';
/* Field: COUNT(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['group_type'] = 'count';
$handler->display->display_options['fields']['created']['label'] = 'Count';
$handler->display->display_options['fields']['created']['suffix'] = ' time(s)';
/* Field: MAX(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created_1']['id'] = 'created_1';
$handler->display->display_options['fields']['created_1']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created_1']['field'] = 'created';
$handler->display->display_options['fields']['created_1']['group_type'] = 'max';
$handler->display->display_options['fields']['created_1']['label'] = 'Most recent';
$handler->display->display_options['fields']['created_1']['date_format'] = 'long';
$handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
/* Field: MIN(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created_2']['id'] = 'created_2';
$handler->display->display_options['fields']['created_2']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created_2']['field'] = 'created';
$handler->display->display_options['fields']['created_2']['group_type'] = 'min';
$handler->display->display_options['fields']['created_2']['label'] = 'First time';
$handler->display->display_options['fields']['created_2']['date_format'] = 'long';
$handler->display->display_options['fields']['created_2']['second_date_format'] = 'long';

/* Display: Top downloads */
$handler = $view->new_display('page', 'Top downloads', 'page_top_downloads');
$handler->display->display_options['path'] = 'admin/reports/ngsl/top';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'NGSL Top downloads';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Top users */
$handler = $view->new_display('page', 'Top users', 'page_top_users');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Nginx securelink top users';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
  'requested_path' => 'requested_path',
  'created' => 'created',
  'created_2' => 'created_2',
  'created_1' => 'created_1',
);
$handler->display->display_options['style_options']['default'] = 'requested_path';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'requested_path' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created_2' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created_1' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['relationships'] = FALSE;
/* Relationship: Nginx securelink: NGSL user */
$handler->display->display_options['relationships']['uid']['id'] = 'uid';
$handler->display->display_options['relationships']['uid']['table'] = 'ngsl_analytics';
$handler->display->display_options['relationships']['uid']['field'] = 'uid';
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: User: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'users';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['relationship'] = 'uid';
$handler->display->display_options['fields']['name']['label'] = 'User';
/* Field: COUNT(DISTINCT Nginx securelink: Requested path) */
$handler->display->display_options['fields']['requested_path']['id'] = 'requested_path';
$handler->display->display_options['fields']['requested_path']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['requested_path']['field'] = 'requested_path';
$handler->display->display_options['fields']['requested_path']['group_type'] = 'count_distinct';
$handler->display->display_options['fields']['requested_path']['label'] = 'Unique downloads';
$handler->display->display_options['fields']['requested_path']['suffix'] = ' file(s)';
/* Field: COUNT(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['group_type'] = 'count';
$handler->display->display_options['fields']['created']['label'] = 'Total downloads';
$handler->display->display_options['fields']['created']['suffix'] = ' download(s)';
/* Field: MIN(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created_2']['id'] = 'created_2';
$handler->display->display_options['fields']['created_2']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created_2']['field'] = 'created';
$handler->display->display_options['fields']['created_2']['group_type'] = 'min';
$handler->display->display_options['fields']['created_2']['label'] = 'First time';
$handler->display->display_options['fields']['created_2']['date_format'] = 'short';
$handler->display->display_options['fields']['created_2']['second_date_format'] = 'long';
/* Field: MAX(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created_1']['id'] = 'created_1';
$handler->display->display_options['fields']['created_1']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created_1']['field'] = 'created';
$handler->display->display_options['fields']['created_1']['group_type'] = 'max';
$handler->display->display_options['fields']['created_1']['label'] = 'Last time';
$handler->display->display_options['fields']['created_1']['date_format'] = 'short';
$handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
$handler->display->display_options['path'] = 'admin/reports/ngsl/top-users';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'NGSL Top users';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;

/* Display: Top referrers */
$handler = $view->new_display('page', 'Top referrers', 'page_top_referrers');
$handler->display->display_options['defaults']['title'] = FALSE;
$handler->display->display_options['title'] = 'Nginx securelink top referrers';
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'referrer' => 'referrer',
  'aid' => 'aid',
  'created_1' => 'created_1',
  'created' => 'created',
);
$handler->display->display_options['style_options']['default'] = 'aid';
$handler->display->display_options['style_options']['info'] = array(
  'referrer' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'aid' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Field: Nginx securelink: Request referrer */
$handler->display->display_options['fields']['referrer']['id'] = 'referrer';
$handler->display->display_options['fields']['referrer']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['referrer']['field'] = 'referrer';
$handler->display->display_options['fields']['referrer']['label'] = 'Referrer';
$handler->display->display_options['fields']['referrer']['empty'] = '- No referrer -';
$handler->display->display_options['fields']['referrer']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['referrer']['hide_alter_empty'] = FALSE;
/* Field: COUNT(Nginx securelink: Record identifier) */
$handler->display->display_options['fields']['aid']['id'] = 'aid';
$handler->display->display_options['fields']['aid']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['aid']['field'] = 'aid';
$handler->display->display_options['fields']['aid']['group_type'] = 'count';
$handler->display->display_options['fields']['aid']['label'] = 'Count';
/* Field: MIN(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created_1']['id'] = 'created_1';
$handler->display->display_options['fields']['created_1']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created_1']['field'] = 'created';
$handler->display->display_options['fields']['created_1']['group_type'] = 'min';
$handler->display->display_options['fields']['created_1']['label'] = 'First time';
$handler->display->display_options['fields']['created_1']['date_format'] = 'long';
$handler->display->display_options['fields']['created_1']['second_date_format'] = 'long';
/* Field: MAX(Nginx securelink: Request date) */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'ngsl_analytics';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['group_type'] = 'max';
$handler->display->display_options['fields']['created']['label'] = 'Last time';
$handler->display->display_options['fields']['created']['date_format'] = 'long';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
$handler->display->display_options['defaults']['filter_groups'] = FALSE;
$handler->display->display_options['defaults']['filters'] = FALSE;
/* Filter criterion: Nginx securelink: Request referrer */
$handler->display->display_options['filters']['referrer']['id'] = 'referrer';
$handler->display->display_options['filters']['referrer']['table'] = 'ngsl_analytics';
$handler->display->display_options['filters']['referrer']['field'] = 'referrer';
$handler->display->display_options['filters']['referrer']['operator'] = 'contains';
$handler->display->display_options['filters']['referrer']['exposed'] = TRUE;
$handler->display->display_options['filters']['referrer']['expose']['operator_id'] = 'referrer_op';
$handler->display->display_options['filters']['referrer']['expose']['label'] = 'Referrer contains';
$handler->display->display_options['filters']['referrer']['expose']['description'] = 'Filter to show only certain referrers.';
$handler->display->display_options['filters']['referrer']['expose']['operator'] = 'referrer_op';
$handler->display->display_options['filters']['referrer']['expose']['identifier'] = 'referrer';
$handler->display->display_options['filters']['referrer']['expose']['remember_roles'] = array(
  2 => '2',
  1 => 0,
  3 => 0,
);
$handler->display->display_options['path'] = 'admin/reports/ngsl/top-referres';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'NGSL Top referrers';
$handler->display->display_options['menu']['weight'] = '0';
$handler->display->display_options['menu']['name'] = 'management';
$handler->display->display_options['menu']['context'] = 0;
$handler->display->display_options['menu']['context_only_inline'] = 0;
