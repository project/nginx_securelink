<?php

/**
 * @file
 * Views integration for ngsl_analytics.module
 *
 * @author Ahmad Hejazee <mngafa@gmail.com>
 */

/**
 * Implements hook_views_data().
 */
function ngsl_analytics_views_data() {
  $data = array();

  $data['ngsl_analytics']['table']['group'] = t('Nginx securelink');
  $data['ngsl_analytics']['table']['base'] = array(
    'field' => 'aid',
    'title' => t('Nginx securelink'),
    'help' => t('Download analytics for nginx securelink link generations.'),
  );

  /**
   * The aid field
   */
  $data['ngsl_analytics']['aid'] = array(
    'title' => t('Record identifier'),
    'help' => t('The unique identifier for analytics record.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  /**
   * The uid field
   */
  $data['ngsl_analytics']['uid'] = array(
    'title' => t('User uid'),
    'help' => t('The uid of the user who requested the link.'),
    'field' => array(
      'handler' => 'views_handler_field_user',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'title' => t('NGSL user'),
      'help' => t('Relate the analytics record to the user who requested the link.'),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'base field' => 'uid',
      'label' => t('NGSL user'),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );

  /**
   * The created field
   */
  $data['ngsl_analytics']['created'] = array(
    'title' => t('Request date'),
    'help' => t('The date the user requested the secure link.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  /**
   * Pseudo argument for the created field
   */
  $data['ngsl_analytics']['created_fulldate'] = array(
    'title' => t('Request full date'),
    'help' => t('Date in the form of CCYYMMDD of when the securelink was requested.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_fulldate',
    ),
  );

  /**
   * Pseudo argument for the created field
   */
  $data['ngsl_analytics']['created_year_month'] = array(
    'title' => t('Request date year + month'),
    'help' => t('Date in the form of YYYYMM of when the securelink was requested.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year_month',
    ),
  );

  /**
   * Pseudo argument for the created field
   */
  $data['ngsl_analytics']['created_year'] = array(
    'title' => t('Request year'),
    'help' => t('Date in the form of YYYY of when the securelink was requested.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_year',
    ),
  );

  /**
   * Pseudo argument for the created field
   */
  $data['ngsl_analytics']['created_month'] = array(
    'title' => t('Request month'),
    'help' => t('Date in the form of MM (01 - 12) of when the securelink was requested.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_month',
    ),
  );

  /**
   * Pseudo argument for the created field
   */
  $data['ngsl_analytics']['created_day'] = array(
    'title' => t('Request day'),
    'help' => t('Date in the form of DD (01 - 31) of when the securelink was requested.'),
    'argument' => array(
      'field' => 'created',
      'handler' => 'views_handler_argument_node_created_day',
    ),
  );

  /**
   * The requested_path field
   */
  $data['ngsl_analytics']['requested_path'] = array(
    'title' => t('Requested path'),
    'help' => t('The path of the file being requested.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  /**
   * The requested_path field
   */
  $data['ngsl_analytics']['referrer'] = array(
    'title' => t('Request referrer'),
    'help' => t('The HTTP referer of the requestlink page.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;
}
